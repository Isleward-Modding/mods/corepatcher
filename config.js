// only log if on main thread for prettyness
if (!process.argv[1].endsWith('worker'))
	console.log('[CorePatcher] Override configuration file loaded');

module.exports = {
	overrides: {
		server: {
			requests: {
				// modified version of default request router
				default: function (req, res) {
					let root = req.url.split('/')[1];
					let file = req.params[0];

					file = file.replace('/' + root + '/', '');

					// this is an example! it'll be annoying in logs, don't use it unless testing that the overriding works.
					//console.log('Sending file ' + file);

					res.sendFile(file, {
						root: '../' + root
					});
				}
			}
		},
		'misc/events': {
			emit: function (event) {
				console.log('Event Fired: ' + event);

				let args = [].slice.call(arguments, 1);

				let list = this.events[event];
				if (!list) {
					this.queue.push({
						event: event,
						args: args
					});

					return;
				}

				let len = list.length;
				for (let i = 0; i < len; i++) {
					let l = list[i];
					l.apply(null, args);
				}
			}
		}
	}
};
