module.exports = {
	name: 'CorePatcher',

	// unused, but just for information to readers of the code
	author: 'notme',
	description: 'Hacky way to override methods in the engine code.',

	extraScripts: [
		'config'
	],

	init: function () {
		if (process.argv[1].endsWith('worker')) {
			console.log('[CorePatcher] WARN can\'t run CorePatcher on map threads, stopping this thread');
			return;
		}

		let tl = this;

		console.log('[CorePatcher] Loading overrides...');
		let overrides = require('./config').overrides;
		this.events.emit('corepatcher:onGetOverrides', overrides);

		console.log('[CorePatcher] Starting overriding process...');
		Object.keys(overrides).forEach(function (key) {
			tl.override(key, []);
		});
		console.log('[CorePatcher] Finished overriding methods!');
	},
	override: function (loc, path) {
		console.log('[CorePatcher] Overriding ' + loc + ':' + (path.join('.') || 'N/A'));

		let config = require('./config').overrides[loc];
		let overrides;
		if (path.length === 0)
			overrides = config;
		else
			overrides = this.getRecursive(require('./config').overrides[loc], path.join('.'));

		if (this.isFunction(overrides)) {
			let obj = require('../../' + loc);
			if (path.length > 1) {
				obj = this.getRecursive(obj, path.slice(0, path.length - 1).join('.'));
				obj[path[path.length - 1]] = overrides;
			} else
				obj[path[0]] = overrides;
		} else {
			let tl = this;
			Object.keys(overrides).forEach(function (key) {
				let clone = path.slice(0);
				clone.push(key);
				tl.override(loc, clone);
			});
		}
	},

	// helpers
	isFunction: function (functionToCheck) {
		return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
	},
	getRecursive: function (obj, str) {
		return str.split('.').reduce(function (o, x) {
			return o[x];
		}, obj);
	}
};
