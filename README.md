# CorePatcher

*Mod by notme#1560*

*Last tested with Isleward v0.2.1*

A simple, hacky mod to allow a config file or other mods to "patch" (overwrite) methods inside the core game.
Won't work if they are core functions that are only called once (and before the mod is loaded), for example, but if the method is used multiple times, future calls will be to the new function.
Additionally, if two mods attempt to override the same method, only one will work... this can be fixed by having a unified mod that just calls methods from other mods (or even emits an event for other mods to hook to) so that all mods can call what they need.
However, if you're only using one or two mods and they don't conflict this will work fine, but remember that this mod is a hacky mess.

## Configuring
The default config file can be used as an example, and comes with helpful changes to the core that won't change much.
The top level config object just has `overrides`.
In `overrides`, the properties are file paths, relative to `/src/server`.
Don't include a leading or trailing slash.
Inside of that property, you can have nested objects to access different properties.
If the value of a nested property is a function, it will overwrite the corresponding method in the core engine code.

## Usage
Option 1: Edit the config file as explained in the first section

Option 2: Listen for the event `corepatcher:onGetOverrides`.
The data of that event is the overrides object described in the config.
Just edit that the same way you would edit the config.
